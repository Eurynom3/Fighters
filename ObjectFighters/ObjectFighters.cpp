// ObjectFighters.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Human.h"
#include "Weapon.h"
#include "Magician.h"
#include "Fighter.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//Instanciation of a weapon with 2 damage and 2 bonus str
	Weapon basicSword = Weapon("BasicSword", 2, 0, 2);
	
	//Instanciation of a Human
	Human jackBasicHuman = Human("Jack", 50, 5, 5);

	// Add a basic sword to jack the basic human
	jackBasicHuman.AddWeapon(basicSword);

	//Instanciate a magician
	Magician magi = Magician("Magi", 1, 10, 40);
	Weapon magicWound = Weapon("wound", 0, 10, 2);
	magi.AddWeapon(magicWound);

	//Instanciate a fighter
	Fighter fifi = Fighter("Fifi", 10, 1, 60);
	fifi.AddWeapon(basicSword);


	//Use the public methods for fight.
	while(fifi.IsAlive() || magi.IsAlive())
	{
		fifi.LoseLife(magi.ThrowFireball());
		magi.LoseLife(fifi.Hit());
	}


	/*
	Goin further:
		- Try to add a Human 
		- Change the weapon of the Fighter
		- Add mana to the Magician, a fireball use 20 mana. 
		- Add a spell to the magician that regen mana. 
		- Add a spell to the fighter that reduce damage taken. (difficult one, think about polymorphism and lose life method)
		- Add a characteristics named Agility to everyone
		- Create a new classe called ranger that use agility

		If you can do all of that Well played.
	*/
	return 0;
}

