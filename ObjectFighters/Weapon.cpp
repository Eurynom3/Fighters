#include "stdafx.h"
#include "Weapon.h"

Weapon::Weapon()
{

}

Weapon::Weapon(std::string name, int bonusStr, int bonusInt, int damage)
{
	_name = name;
	_bonusStrenght = bonusStr;
	_bonusIntelligence = bonusInt;
	_damage = damage;
}


Weapon::~Weapon(void)
{
}

int Weapon::GetBonusInt()
{
	return _bonusIntelligence;
}

int Weapon::GetBonusStrenght()
{
	return _bonusStrenght;
}

int Weapon::GetDamage()
{
	return _damage;
}