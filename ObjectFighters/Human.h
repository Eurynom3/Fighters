#pragma once

#include "Weapon.h"

class Human
{
public:
	Human(std::string name, int lifePoint, int strenght, int intelligence);
	~Human(void);
	
	//Return the damage of a normal hit with the weapon.
	int Hit();

	//Return if the character is still alive.
	bool IsAlive();

	//Reduce lifepoint of the character
	void LoseLife(int damageTaken);

	//Return the full strenght weapon+base of a player
	int GetStrenght();

	//Change to the desire weapon
	void AddWeapon(Weapon weapon);

private:
	//Name of the Character
	std::string _name;

	//Actual life point of my character
	int _lifePoint;


protected:
	//Weapon the character is using.
	Weapon _weapon;

	//Strenght of the character
	int _strenght;

	//Intelligence of the character
	int _intelligence;


};

