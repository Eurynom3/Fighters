#include "stdafx.h"
#include "Human.h"


Human::Human(std::string name, int lifePoint, int strenght, int intelligence)
{
	_name = name;
	_lifePoint = lifePoint;
	_strenght = strenght;
	_intelligence = intelligence;
}

Human::~Human(void)
{
}

int Human::Hit()
{
	return _weapon.GetDamage() + GetStrenght();
}

int Human::GetStrenght()
{
	return _weapon.GetBonusStrenght() + _strenght;
}

void Human::LoseLife(int damageTaken)
{
	_lifePoint -= damageTaken;
}

bool Human::IsAlive()
{
	return _lifePoint <= 0 ? false : true;
}

void Human::AddWeapon(Weapon weapon)
{
	_weapon = weapon;
}