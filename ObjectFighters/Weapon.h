#pragma once
class Weapon
{
public:
	Weapon();
	Weapon(std::string name, int bonusStr, int bonusInt, int damage);
	~Weapon(void);

	int GetBonusInt();

	int GetBonusStrenght();

	int GetDamage();

private:
	std::string _name;

	int _bonusStrenght;

	int _bonusIntelligence;

	int _damage;
};

